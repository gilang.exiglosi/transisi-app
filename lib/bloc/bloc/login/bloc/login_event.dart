part of 'login_bloc.dart';

class LoginEvent extends Equatable {
  const LoginEvent();

  @override
  List<Object> get props => [];
}

class OnChangeEmail extends LoginEvent {
  final String val;

  OnChangeEmail({this.val});
}

class LoginSubmit extends LoginEvent {}

class OnChangeObsecure extends LoginEvent {}
