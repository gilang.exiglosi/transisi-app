import 'dart:async';
import 'dart:convert';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:transisi_app/config/service.dart';
import 'package:transisi_app/key/keys.dart';
import 'package:transisi_app/utils/cache.dart';
import 'package:transisi_app/utils/form_validation.dart';

part 'login_event.dart';
part 'login_state.dart';

class LoginBloc extends Bloc<LoginEvent, LoginState> {
  LoginBloc() : super(LoginInitial());

  @override
  Stream<LoginState> mapEventToState(
    LoginEvent event,
  ) async* {
    if (event is OnChangeEmail) {
      var resultHashmap = FormValidationHelper.validateEmail(state.email);
      if (resultHashmap.containsKey(true)) {
        if (resultHashmap.containsValue("Email Can\'t be empty")) {
          state.invalidEmail = true;
          state.errorEmail = "Email Can\'t be empty";
        }
        if (resultHashmap.containsValue("Invalid email format")) {
          state.invalidEmail = true;
          state.errorEmail = "Invalid email format";
        }
      } else if (resultHashmap.containsKey(false)) {
        state.invalidEmail = false;
      }
      yield state.copyWith(LoginState(
          invalidPassword: state.invalidPassword,
          invalidEmail: state.invalidEmail,
          errorEmail: state.errorEmail));
    } else if (event is LoginSubmit) {
      final body = {"email": state.email, "password": state.password};

      String res = await ServiceConfig.reqPost("api/login", body);
      var data = json.decode(res);

      if (data['token'] != null) {
        Cache.setCache(key: GlobalKeys.token, data: data['token']);
        yield LoginSuccess("Success");
      } else {
        yield LoginFailed(data['error']);
      }
    } else if (event is OnChangeObsecure) {
      yield state.copyWith(LoginState(invalidPassword: !state.invalidPassword));
    }
  }
}
