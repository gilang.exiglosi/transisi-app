part of 'login_bloc.dart';

class LoginState {
  String email;
  String password;
  String errorEmail;
  String errorPassword;
  bool invalidEmail;
  bool invalidPassword;

  LoginState(
      {this.errorEmail,
      this.errorPassword,
      this.invalidEmail = false,
      this.invalidPassword = false,
      this.email,
      this.password});

  LoginState copyWith(LoginState loginState) {
    return LoginState(
        email: loginState.email ?? email,
        invalidEmail: loginState.invalidEmail ?? email,
        invalidPassword: loginState.invalidPassword ?? invalidPassword,
        errorEmail: loginState.errorEmail ?? errorEmail,
        errorPassword: loginState.errorPassword ?? errorPassword,
        password: loginState.password ?? password);
  }
}

class LoginInitial extends LoginState {}

class LoginSuccess extends LoginState {
  final String message;

  LoginSuccess(this.message);
}

class LoginFailed extends LoginState {
  final String message;

  LoginFailed(this.message);
}
