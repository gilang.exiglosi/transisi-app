import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:transisi_app/config/service.dart';

part 'employee_event.dart';
part 'employee_state.dart';

class EmployeeBloc extends Bloc<EmployeeEvent, EmployeeState> {
  EmployeeBloc() : super(EmployeeInitial());

  @override
  Stream<EmployeeState> mapEventToState(
    EmployeeEvent event,
  ) async* {
    if (event is OnSubmit) {
      yield* mapSubmitEventToState();
    } else if (event is OnChangeTextField) {
      yield* mapNameEventToState();
    }
  }

  Stream<EmployeeState> mapSubmitEventToState() async* {
    yield EmployeeLoading();

    final body = {"name": state.name, "job": state.job};

    String res = await ServiceConfig.reqPost("api/users", body);
    print(res);
    if (res == "400") {
      yield CreateFailed("Failed");
    } else {
      yield CreateSuccess("Success");
    }
  }

  Stream<EmployeeState> mapNameEventToState() async* {
    if (state.name != null && state.job != null) {
      state.invalidname = false;
      state.invalidjob = false;
    } else if (state.name == null && state.job == null) {
      state.invalidname = true;
      state.invalidjob = true;
      state.errorjob = "Job Can't be Empty";
      state.errorname = "Name Can't be Empty";
    } else if (state.name == null) {
      state.invalidname = true;
      state.errorname = "Name Can't be Empty";
    } else if (state.name != null) {
      state.invalidname = false;
    } else if (state.job != null) {
      state.invalidjob = false;
    } else if (state.job == null) {
      state.invalidjob = true;
      state.errorjob = "Job Can't be Empty";
    }

    yield state.copyWith(EmployeeState(
        job: state.job,
        name: state.name,
        errorjob: state.errorjob,
        invalidjob: state.invalidjob,
        invalidname: state.invalidname,
        errorname: state.errorname));
  }
}
