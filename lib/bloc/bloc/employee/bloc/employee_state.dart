part of 'employee_bloc.dart';

class EmployeeState {
  String name;
  String job;
  String errorname;
  String errorjob;
  bool invalidname;
  bool invalidjob;

  EmployeeState({
    this.name,
    this.job,
    this.errorname,
    this.errorjob,
    this.invalidname = false,
    this.invalidjob = false,
  });

  EmployeeState copyWith(EmployeeState state) {
    return EmployeeState(
      name: state.name ?? name,
      job: state.job ?? job,
      errorjob: state.errorjob ?? errorjob,
      errorname: state.errorname ?? errorname,
      invalidjob: state.invalidjob ?? invalidjob,
      invalidname: state.invalidname ?? invalidname,
    );
  }
}

class EmployeeInitial extends EmployeeState {}

class EmployeeLoading extends EmployeeState {}

class CreateSuccess extends EmployeeState {
  final String message;

  CreateSuccess(this.message);
}

class CreateFailed extends EmployeeState {
  final String message;

  CreateFailed(this.message);
}
