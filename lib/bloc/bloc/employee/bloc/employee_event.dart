part of 'employee_bloc.dart';

abstract class EmployeeEvent extends Equatable {
  const EmployeeEvent();

  @override
  List<Object> get props => [];
}

class OnSubmit extends EmployeeEvent {}

class OnChangeJob extends EmployeeEvent {}

class OnChangeTextField extends EmployeeEvent {}
