part of 'authentication_bloc.dart';

enum AuthenticationStatus { unknown, authenticated, unauthenticated }

@immutable
class AuthenticationState extends Equatable {
  const AuthenticationState._({
    this.status = AuthenticationStatus.unknown,
  });
  const AuthenticationState.authenticated()
      : this._(status: AuthenticationStatus.authenticated);

  const AuthenticationState.unauthenticated()
      : this._(
          status: AuthenticationStatus.unauthenticated,
        );
  const AuthenticationState.unknown() : this._();

  final AuthenticationStatus status;
  @override
  List<Object> get props => [status];
}

class AuthenticationInitial extends AuthenticationState {
  AuthenticationInitial.unauthenticated() : super.unauthenticated();
}
