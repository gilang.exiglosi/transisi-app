import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';
import 'package:transisi_app/key/keys.dart';
import 'package:transisi_app/utils/cache.dart';

part 'authentication_event.dart';
part 'authentication_state.dart';

class AuthenticationBloc
    extends Bloc<AuthenticationEvent, AuthenticationState> {
  AuthenticationBloc() : super(AuthenticationState.unknown());

  @override
  Stream<AuthenticationState> mapEventToState(
    AuthenticationEvent event,
  ) async* {
    if (event is AuthenticationStatusChanged) {
      yield await _mapAuthenticationStatusChangedToState(event);
    }
  }

  Future<AuthenticationState> _mapAuthenticationStatusChangedToState(
    event,
  ) async {
    String token = await Cache.getCache(
      key: GlobalKeys.token,
    );

    if (token == "") {
      return AuthenticationState.unauthenticated();
    } else {
      return AuthenticationState.authenticated();
    }
  }
}
