import 'dart:async';
import 'dart:convert';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:transisi_app/config/service.dart';
import 'package:transisi_app/model/employee_model.dart';
import 'package:transisi_app/utils/cache.dart';

part 'home_event.dart';
part 'home_state.dart';

class HomeBloc extends Bloc<HomeEvent, HomeState> {
  HomeBloc() : super(HomeInitial());

  @override
  Stream<HomeState> mapEventToState(
    HomeEvent event,
  ) async* {
    if (event is GetListEmployee) {
      yield* mapGetlistEmployeeEventToState();
    } else if (event is OnChangeSearch) {
      yield* mapSearchEmployeeEventToState();
    } else if (event is Logout) {
      yield* mapLogoutEventToState();
    }
  }

  Stream<HomeState> mapLogoutEventToState() async* {
    Cache.clearCache();
    yield Success("Success");
  }

  Stream<HomeState> mapGetlistEmployeeEventToState() async* {
    String res = await ServiceConfig.reqGet("api/users?per_page=20", {});
    EmployeeModel employeeModel = EmployeeModel.fromJson(json.decode(res));
    yield OnListEmployee(employeeModel: employeeModel);
  }

  Stream<HomeState> mapSearchEmployeeEventToState() async* {
    List<Datum> data = [];

    EmployeeModel employeeModel = state.employeeModel;
    for (int i = 0; i < state.employeeModel.data.length; i++) {
      Datum name = state.employeeModel.data.elementAt(i);
      if (name.firstName.toLowerCase().contains(state.query.toLowerCase())) {
        data.add(name);
      }
    }

    data.map((e) {
      print(e.firstName);
    }).toList();

    yield OnListEmployee(
      employeeModel: employeeModel,
      data: data,
    );
  }
}
