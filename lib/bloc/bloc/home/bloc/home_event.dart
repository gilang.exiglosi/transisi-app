part of 'home_bloc.dart';

abstract class HomeEvent extends Equatable {
  const HomeEvent();

  @override
  List<Object> get props => [];
}

class GetListEmployee extends HomeEvent {}

class OnChangeSearch extends HomeEvent {}

class Logout extends HomeEvent {}
