part of 'home_bloc.dart';

class HomeState {
  final EmployeeModel employeeModel;
  String query;
  List<Datum> data;

  HomeState({this.query, this.data, this.employeeModel});
}

class HomeInitial extends HomeState {}

class OnListEmployee extends HomeState {
  final EmployeeModel employeeModel;
  List<Datum> data;
  String query;
  OnListEmployee({
    this.employeeModel,
    this.data,
    this.query,
  }) : super(query: query, data: data, employeeModel: employeeModel);
}

class Success extends HomeState {
  final String message;

  Success(this.message);
}
