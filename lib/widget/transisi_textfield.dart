import 'package:flutter/material.dart';

class TransisiTextField extends StatelessWidget {
  final String labelText;
  final String errorText;
  final String initialValue;
  final Icon prefixIcon;
  final Widget suffixIcon;
  final TextEditingController controller;
  final bool obscureText;
  final bool readOnly;
  final double height;
  final double width;
  final EdgeInsetsGeometry margin;
  final FormFieldValidator<String> validator;
  final VoidCallback onTap;
  final Function(String data) onChanged;
  final FocusNode focusNode;
  final FocusNode nextFocus;
  final bool wordsCap;
  final TextInputAction textInputAction;
  final ValueChanged<String> onFieldSubmitted;

  TransisiTextField(
      {this.labelText,
      this.prefixIcon,
      this.initialValue,
      this.suffixIcon,
      this.errorText,
      this.controller,
      this.obscureText,
      this.height,
      this.width,
      this.margin,
      this.validator,
      this.readOnly = false,
      this.onTap,
      this.onChanged,
      this.focusNode,
      this.nextFocus,
      this.wordsCap = false,
      this.textInputAction,
      this.onFieldSubmitted});

  @override
  Widget build(BuildContext context) {
    return Container(
      width: width,
      height: height,
      margin: margin,
      child: TextFormField(
        initialValue: initialValue,
        textCapitalization:
            (wordsCap) ? TextCapitalization.words : TextCapitalization.none,
        textInputAction: textInputAction,
        obscureText: obscureText,
        controller: controller,
        readOnly: readOnly,
        onTap: onTap,
        onChanged: onChanged,
        focusNode: focusNode,
        onFieldSubmitted: onFieldSubmitted,
        decoration: InputDecoration(
            errorText: errorText,
            contentPadding: EdgeInsets.all(10.0),
            isDense: true,
            filled: true,
            fillColor: Color(0xffFCFAFA),
            enabledBorder:
                OutlineInputBorder(borderRadius: BorderRadius.circular(8.0)),
            border: OutlineInputBorder(
                borderSide: BorderSide(),
                borderRadius: BorderRadius.circular(8.0)),
            errorBorder: OutlineInputBorder(
              borderRadius: BorderRadius.circular(10.0),
            ),
            focusedErrorBorder: OutlineInputBorder(
              borderRadius: BorderRadius.circular(10.0),
            ),
            labelText: labelText,
            focusedBorder:
                OutlineInputBorder(borderRadius: BorderRadius.circular(8.0)),
            prefixIcon: prefixIcon,
            suffixIcon: suffixIcon),
      ),
    );
  }
}
