import 'package:flutter/material.dart';

class TransisiTile extends StatelessWidget {
  final widget;
  final String title;
  final String subtitle;
  const TransisiTile({Key key, this.title, this.subtitle, this.widget})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Card(
      child: Container(
        height: 50,
        padding: EdgeInsets.all(8.0),
        child: Row(
          children: [
            Container(
              child: CircleAvatar(
                radius: 40.0,
                backgroundColor: Colors.white,
                child: CircleAvatar(
                  radius: 38.0,
                  child: widget,
                  backgroundColor: Color(0xff5a32a8),
                ),
              ),
            ),
            Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  title,
                  style: TextStyle(fontSize: 14),
                ),
                Text(
                  subtitle,
                  style: TextStyle(color: Colors.grey),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
