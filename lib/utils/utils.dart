import 'package:flutter/material.dart';
import 'package:toast/toast.dart';

abstract class Utils {
  static void myToast(context, String msg, {int duration}) {
    print('### myToast() -> $msg');
    Toast.show(msg, context, duration: 1, gravity: Toast.BOTTOM);
  }

  static void mySnackBar(context, String msg, color) {
    print('### mySnackBar() -> $msg');
    ScaffoldMessenger.of(context)
      ..hideCurrentSnackBar()
      ..showSnackBar(
        SnackBar(backgroundColor: color, content: Text(msg)),
      );
  }

  static void myPushPage(Widget pagePush, BuildContext context) {
    Navigator.push(context, MaterialPageRoute(builder: (context) {
      print('### myPushPage() -> $pagePush');
      return pagePush;
    }));
  }

  static void myPushPageReplacement(Widget pagePush, context) {
    Navigator.pushReplacement(context, MaterialPageRoute(builder: (contexts) {
      print('### myPushPageReplacement() -> $pagePush');
      return pagePush;
    }));
  }

  static void myPushAndRemoveUntil(Widget pagePush, context) {
    Navigator.pushAndRemoveUntil(
        context,
        MaterialPageRoute(builder: (BuildContext context) => pagePush),
        (Route<dynamic> route) => false);
  }
}
