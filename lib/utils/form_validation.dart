class FormValidationHelper {
  static Map<bool, String> validateEmail(String input) {
    var resultHashmap = <bool, String>{};
    bool _validEmailFormat = RegExp(
            r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+")
        .hasMatch(input);
    if ((input.isEmpty == false) && (_validEmailFormat)) {
      resultHashmap[false] = "";
    } else if (input.isEmpty) {
      resultHashmap[true] = "Email Can\'t be empty";
    } else if (_validEmailFormat == false) {
      resultHashmap[true] = "Invalid email format";
    }
    return resultHashmap;
  }
}
