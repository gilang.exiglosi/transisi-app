import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:transisi_app/key/keys.dart';

class ServiceConfig {
  static final headers = {"Content-Type": "application/json"};

  static Future<String> reqPost(
    endPoint,
    body,
  ) async {
    final uri = Uri.parse(GlobalKeys.baseUrl + endPoint);
    body = json.encode(body);

    Map<String, String> headers = {
      'Content-type': 'application/json',
    };
    try {
      final res = await http.post(uri, headers: headers, body: body);
      print(
          '### reqPost() ${GlobalKeys.baseUrl + endPoint} -- $body-> ${res.body}' +
              headers.toString());
      return res.body.toString();
    } catch (e) {
      print(e);
      return null;
    }
  }

  static Future<String> reqGet(
    endPoint,
    body,
  ) async {
    final uri = Uri.parse(GlobalKeys.baseUrl + endPoint);

    body = json.encode(body);
    Map<String, String> headers = {
      'Content-type': 'application/json',
    };
    try {
      final res = await http.get(
        uri,
        headers: headers,
      );
      print(
          '### reqGet() ${GlobalKeys.baseUrl + endPoint} -- $body-> ${res.body}' +
              headers.toString());
      return res.body.toString();
    } catch (e) {
      print(e);
      return null;
    }
  }
}
