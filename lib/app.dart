import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:transisi_app/bloc/bloc/authentication_bloc.dart';
import 'package:transisi_app/view/home.dart';

import 'view/login_ui.dart';

class MyApp extends StatefulWidget {
  MyApp({Key key}) : super(key: key);

  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => AuthenticationBloc()
        ..add(
            AuthenticationStatusChanged(AuthenticationStatus.unauthenticated)),
      child: MaterialApp(
        theme: ThemeData(
          brightness: Brightness.light,
          primaryColor: Color(0xff5a32a8), // Color(0xff91c749)
          accentColor: Color(0xff5a32a8),
        ),
        home: BlocBuilder<AuthenticationBloc, AuthenticationState>(
          builder: (ctx, state) {
            switch (state.status) {
              case AuthenticationStatus.authenticated:
                return HomeUi();
                break;
              case AuthenticationStatus.unauthenticated:
                return LoginUi();
                break;
              default:
                return CircularProgressIndicator();

                break;
            }
          },
        ),
      ),
    );
  }
}
