import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:transisi_app/bloc/bloc/employee/bloc/employee_bloc.dart';
import 'package:transisi_app/utils/utils.dart';
import 'package:transisi_app/widget/transisi_textfield.dart';

class CreateEmployee extends StatelessWidget {
  const CreateEmployee({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        automaticallyImplyLeading: true,
        elevation: 0,
      ),
      body: BlocProvider(
        create: (context) => EmployeeBloc(),
        child: BlocListener<EmployeeBloc, EmployeeState>(
          listener: (context, state) {
            if (state is CreateSuccess) {
              Utils.myToast(context, state.message);
              Navigator.of(context).pop();
            } else if (state is CreateFailed) {
              Utils.myToast(context, state.message);
            }
          },
          child: BlocBuilder<EmployeeBloc, EmployeeState>(
            builder: (context, state) {
              if (state is EmployeeLoading) {
                return Center(
                  child: Container(
                    child: CircularProgressIndicator(),
                  ),
                );
              }
              return Center(
                child: Container(
                  padding: EdgeInsets.all(8.0),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      TransisiTextField(
                        labelText: "Name",
                        obscureText: false,
                        onChanged: (val) {
                          state.name = val;
                          context.read<EmployeeBloc>()
                            ..add(OnChangeTextField());
                        },
                        errorText: state.invalidname ? state.errorname : null,
                      ),
                      SizedBox(
                        height: 50,
                      ),
                      TransisiTextField(
                        labelText: "Job",
                        obscureText: false,
                        errorText: state.invalidjob ? state.errorjob : null,
                        onChanged: (val) {
                          state.job = val;
                          context.read<EmployeeBloc>()
                            ..add(OnChangeTextField());
                        },
                      ),
                      ElevatedButton(
                          onPressed: () {
                            (state.name != null && state.job != null)
                                ? context.read<EmployeeBloc>().add(OnSubmit())
                                : context
                                    .read<EmployeeBloc>()
                                    .add(OnChangeTextField());
                          },
                          child: Container(
                            height: 50.0,
                            width: MediaQuery.of(context).size.width / 2,
                            child: Center(child: Text("Create")),
                          ))
                    ],
                  ),
                ),
              );
            },
          ),
        ),
      ),
    );
  }
}
