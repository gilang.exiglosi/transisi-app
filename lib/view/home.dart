import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:transisi_app/bloc/bloc/authentication_bloc.dart';
import 'package:transisi_app/bloc/bloc/home/bloc/home_bloc.dart';
import 'package:transisi_app/utils/utils.dart';
import 'package:transisi_app/view/create_employee.dart';
import 'package:transisi_app/view/list_employee_ui.dart';

class HomeUi extends StatefulWidget {
  const HomeUi({Key key}) : super(key: key);

  @override
  _HomeUiState createState() => _HomeUiState();
}

class _HomeUiState extends State<HomeUi> {
  Widget appBarTitle = new Text(
    "Contact",
    style: new TextStyle(color: Colors.white),
  );
  Icon actionIcon = new Icon(
    Icons.search,
    color: Colors.white,
  );
  final TextEditingController _searchQuery = new TextEditingController();

  bool isSearching;

  @override
  void initState() {
    super.initState();
    isSearching = false;
  }

  handleClick(BuildContext context, value) {
    switch (value) {
      case 'Logout':
        context.read<HomeBloc>().add(Logout());
        break;
    }
  }

  Widget buildBar(BuildContext context, HomeState state) {
    return new AppBar(
        leading: PopupMenuButton<String>(
          onSelected: (val) {
            handleClick(context, val);
          },
          itemBuilder: (BuildContext context) {
            return {
              'Logout',
            }.map((String choice) {
              return PopupMenuItem<String>(
                value: choice,
                child: Text(choice),
              );
            }).toList();
          },
        ),
        title: appBarTitle,
        actions: <Widget>[
          new IconButton(
            icon: actionIcon,
            onPressed: () {
              setState(() {
                if (this.actionIcon.icon == Icons.search) {
                  this.actionIcon = new Icon(
                    Icons.close,
                    color: Colors.white,
                  );
                  this.appBarTitle = new TextField(
                    controller: _searchQuery,
                    onChanged: (val) {
                      state.query = val;
                      context.read<HomeBloc>().add(OnChangeSearch());
                    },
                    style: new TextStyle(
                      color: Colors.white,
                    ),
                    decoration: new InputDecoration(
                        prefixIcon: new Icon(Icons.search, color: Colors.white),
                        hintText: "Search...",
                        hintStyle: new TextStyle(color: Colors.white)),
                  );
                  _handleSearchStart();
                } else {
                  _handleSearchEnd(context, state);
                }
              });
            },
          ),
        ]);
  }

  void _handleSearchStart() {
    setState(() {
      isSearching = true;
    });
  }

  void _handleSearchEnd(BuildContext context, HomeState state) {
    setState(() {
      state.data = null;

      context.read<HomeBloc>().add(OnChangeSearch());
      this.actionIcon = new Icon(
        Icons.search,
        color: Colors.white,
      );
      this.appBarTitle = new Text(
        "Contact",
        style: new TextStyle(color: Colors.white),
      );
      isSearching = false;
      _searchQuery.clear();
    });
  }

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => HomeBloc()..add(GetListEmployee()),
      child: BlocListener<HomeBloc, HomeState>(
        listener: (context, state) {
          if (state is Success) {
            context.read<AuthenticationBloc>()
              ..add(AuthenticationStatusChanged(
                  AuthenticationStatus.unauthenticated));
          }
        },
        child: BlocBuilder<HomeBloc, HomeState>(
          builder: (context, state) {
            if (state is HomeInitial) {
              return Container(
                color: Colors.white,
                child: Center(child: CircularProgressIndicator()),
              );
            }

            return Scaffold(
              appBar: buildBar(context, state),
              floatingActionButton: FloatingActionButton(
                child: Icon(Icons.add),
                onPressed: () {
                  Utils.myPushPage(CreateEmployee(), context);
                },
              ),
              body: Container(
                padding: EdgeInsets.all(8.0),
                child: ListView.builder(
                    itemCount: (state.data != null)
                        ? state.data.length
                        : state.employeeModel.data.length,
                    itemBuilder: (context, index) {
                      return ListEmployeeUI(
                        datum: (state.data != null)
                            ? state.data[index]
                            : state.employeeModel.data[index],
                      );
                    }),
              ),
            );
          },
        ),
      ),
    );
  }
}
