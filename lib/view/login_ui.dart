import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:transisi_app/bloc/bloc/authentication_bloc.dart';
import 'package:transisi_app/bloc/bloc/login/bloc/login_bloc.dart';
import 'package:transisi_app/utils/utils.dart';
import 'package:transisi_app/widget/transisi_textfield.dart';

class LoginUi extends StatefulWidget {
  static Route route() {
    return MaterialPageRoute<void>(builder: (_) => LoginUi());
  }

  LoginUi({Key key}) : super(key: key);

  @override
  _LoginUiState createState() => _LoginUiState();
}

class _LoginUiState extends State<LoginUi> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: BlocProvider(
        create: (context) => LoginBloc(),
        child: BlocListener<LoginBloc, LoginState>(
          listener: (context, state) {
            if (state is LoginSuccess) {
              context.read<AuthenticationBloc>().add(
                  AuthenticationStatusChanged(
                      AuthenticationStatus.authenticated));
              Utils.mySnackBar(context, state.message, Colors.green);
            } else if (state is LoginFailed) {
              Utils.mySnackBar(context, state.message, Colors.red);
            }
          },
          child: BlocBuilder<LoginBloc, LoginState>(
            builder: (context, state) {
              return Center(
                child: Container(
                  padding: EdgeInsets.all(8.0),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      TransisiTextField(
                        labelText: "Email",
                        obscureText: false,
                        onChanged: (val) {
                          state.email = val;
                          context.read<LoginBloc>()..add(OnChangeEmail());
                        },
                        errorText: state.invalidEmail ? state.errorEmail : null,
                      ),
                      SizedBox(
                        height: 50,
                      ),
                      TransisiTextField(
                        labelText: "Password",
                        obscureText: !state.invalidPassword,
                        onChanged: (val) {
                          state.password = val;
                          context.read<LoginBloc>()..add(OnChangeEmail());
                        },
                        suffixIcon: IconButton(
                            onPressed: () {
                              context.read<LoginBloc>()
                                ..add(OnChangeObsecure());
                            },
                            icon: Icon(!state.invalidPassword
                                ? Icons.visibility_off
                                : Icons.visibility)),
                      ),
                      SizedBox(
                        height: 50,
                      ),
                      ElevatedButton(
                          onPressed: () {
                            (state.email != null && state.password != null)
                                ? context.read<LoginBloc>().add(LoginSubmit())
                                : context
                                    .read<LoginBloc>()
                                    .add(OnChangeEmail());
                          },
                          child: Container(
                            height: 50.0,
                            width: MediaQuery.of(context).size.width / 2,
                            child: Center(child: Text("Login")),
                          ))
                    ],
                  ),
                ),
              );
            },
          ),
        ),
      ),
    );
  }
}
