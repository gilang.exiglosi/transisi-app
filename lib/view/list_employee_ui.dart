import 'package:flutter/material.dart';
import 'package:transisi_app/model/employee_model.dart';
import 'package:transisi_app/utils/utils.dart';
import 'package:transisi_app/view/detail_employee.dart';
import 'package:transisi_app/widget/transisi_tile.dart';

class ListEmployeeUI extends StatelessWidget {
  final Datum datum;
  const ListEmployeeUI({Key key, this.datum}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: InkWell(
        onTap: () {
          Utils.myPushPage(DetailEmployeee(datum: datum), context);
        },
        child: TransisiTile(
          title: datum.firstName,
          subtitle: datum.email,
          widget: Container(
            child: CircleAvatar(
              radius: 40.0,
              backgroundColor: Colors.white,
              child: CircleAvatar(
                radius: 38.0,
                child: Text(datum.firstName[0]),
                backgroundColor: Color(0xff5a32a8),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
