import 'package:flutter/material.dart';
import 'package:transisi_app/model/employee_model.dart';
import 'package:transisi_app/widget/transisi_tile.dart';

class DetailEmployeee extends StatelessWidget {
  final Datum datum;

  const DetailEmployeee({Key key, this.datum}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        automaticallyImplyLeading: true,
        elevation: 0,
      ),
      body: Container(
        child: Column(
          children: [
            Container(
              height: MediaQuery.of(context).size.height / 2,
              color: Color(0xff5a32a8),
              child: Center(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Container(
                      child: CircleAvatar(
                        radius: 80.0,
                        backgroundImage: NetworkImage(datum.avatar),
                        backgroundColor: Color(0xff5a32a8),
                      ),
                    ),
                    SizedBox(
                      height: 50,
                    ),
                    Text(
                      datum.firstName,
                      style: TextStyle(
                          fontSize: 22,
                          color: Colors.white,
                          fontWeight: FontWeight.bold),
                    )
                  ],
                ),
              ),
            ),
            TransisiTile(
                title: datum.lastName,
                subtitle: "Last Name",
                widget: Icon(
                  Icons.person,
                  size: 20,
                  color: Colors.white,
                )),
            TransisiTile(
                title: datum.email,
                subtitle: "Email",
                widget: Icon(
                  Icons.mail,
                  size: 20,
                  color: Colors.white,
                )),
          ],
        ),
      ),
    );
  }
}
